//
//  TextIndexedSet.cpp
//  zppsim
//
//  Created by Ed Baskerville on 5/21/14.
//  Copyright (c) 2014 Ed Baskerville. All rights reserved.
//

#include "catch.hpp"
#include "zppsim_random.hpp"
#include "IndexedSet.hpp"

using namespace std;
using namespace zppsim;

TEST_CASE("IndexedSet behaves properly", "[IndexedSet]")
{
	rng_t rng(100);
	
	SECTION("Empty set") {
		IndexedSet<size_t> emptySet;
		REQUIRE(emptySet.size() == 0);
		REQUIRE(!emptySet.contains(0));
		REQUIRE(emptySet.indexOf(0) == 0);
		emptySet.verify();
	}
	
	SECTION("Add a single item") {
		IndexedSet<size_t> x;
		x.insert(1);
		REQUIRE(x.size() == 1);
		REQUIRE(x.contains(1));
		REQUIRE(!x.contains(0));
		REQUIRE(x.indexOf(1) == 0);
		REQUIRE(x.itemAtIndex(0) == 1);
		x.verify();
	}
	
	SECTION("Add and remove a single item") {
		IndexedSet<size_t> x;
		x.insert(1);
		x.remove(1);
		REQUIRE(x.size() == 0);
		REQUIRE(!x.contains(1));
		REQUIRE(x.indexOf(1) == 0);
		x.verify();
	}
	
	SECTION("Add two items and remove first one") {
		IndexedSet<size_t> x;
		x.insert(0);
		x.insert(1);
		
		REQUIRE(x.size() == 2);
		REQUIRE(x.contains(0));
		REQUIRE(x.contains(1));
		REQUIRE(x.indexOf(0) == 0);
		REQUIRE(x.indexOf(1) == 1);
		REQUIRE(x.itemAtIndex(0) == 0);
		REQUIRE(x.itemAtIndex(1) == 1);
		x.verify();
		
		x.remove(0);
		REQUIRE(x.size() == 1);
		REQUIRE(!x.contains(0));
		REQUIRE(x.contains(1));
		REQUIRE(x.indexOf(0) == 1);
		REQUIRE(x.indexOf(1) == 0);
		REQUIRE(x.itemAtIndex(0) == 1);
		x.verify();
	}
}
