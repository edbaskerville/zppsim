//
//  TestIndexedPriorityQueue.cpp
//  malariamodel
//
//  Created by Ed Baskerville on 2/21/14.
//  Copyright (c) 2014 Ed Baskerville. All rights reserved.
//

#include "catch.hpp"
#include "zppsim_random.hpp"
#include "IndexedPriorityQueue.hpp"

using namespace std;
using namespace zppsim;

typedef IndexedPriorityQueueProbe<size_t, SIZE_MAX> SizeTPriorityQueueProbe;

class IntValue
{
public:
	int * ptr;
};

class CompareIntValuePtr
{
public:
	bool operator()(IntValue * const o1, IntValue * const o2)
	{
		return compareInt(*o1->ptr, *o2->ptr);
	}
private:
	std::less<int> compareInt;
};

typedef IndexedPriorityQueueProbe<IntValue *, (IntValue *)nullptr, CompareIntValuePtr> IntValuePriorityQueueProbe;

TEST_CASE("IndexedPriorityQueue behaves properly", "[IndexedPriorityQueue]")
{
	rng_t rng(100);
	
	SECTION("Simple initial heapify.") {
		size_t preHeap[] = {6, 0, 1, 2, 3, 4, 5};
		SizeTPriorityQueueProbe probe(vector<size_t>(begin(preHeap), end(preHeap)));
		probe.heapifyDown(0);
		
		size_t postHeap[] = {0, 2, 1, 6, 3, 4, 5};
		REQUIRE(probe.getHeap() == vector<size_t>(begin(postHeap), end(postHeap)));
	}
	
	SECTION("Incomplete heapify.") {
		size_t preHeap[] = {3, 0, 1, 2};
		SizeTPriorityQueueProbe probe(vector<size_t>(begin(preHeap), end(preHeap)));
		probe.heapifyDown(0);
		
		size_t postHeap[] = {0, 2, 1, 3};
		REQUIRE(probe.getHeap() == vector<size_t>(begin(postHeap), end(postHeap)));
	}
	
	SECTION("Nonzero index with nothing to do.") {
		size_t preHeap[] = {5, 0, 1, 2, 3, 4};
		SizeTPriorityQueueProbe probe(vector<size_t>(begin(preHeap), end(preHeap)));
		probe.heapifyDown(3);
		
		size_t postHeap[] = {5, 0, 1, 2, 3, 4};
		REQUIRE(probe.getHeap() == vector<size_t>(begin(postHeap), end(postHeap)));
	}
	
	SECTION("Nonzero index with something to do.") {
		size_t preHeap[] = {0, 6, 1, 2, 3, 4, 5};
		SizeTPriorityQueueProbe probe(vector<size_t>(begin(preHeap), end(preHeap)));
		probe.heapifyDown(1);
		
		size_t postHeap[] = {0, 2, 1, 6, 3, 4, 5};
		REQUIRE(probe.getHeap() == vector<size_t>(begin(postHeap), end(postHeap)));
	}
	
	SECTION("Borderline index heapify down.") {
		size_t preHeap[] = {0, 1, 6, 2, 3, 4, 5};
		SizeTPriorityQueueProbe probe(vector<size_t>(begin(preHeap), end(preHeap)));
		probe.heapifyDown(2);
		
		size_t postHeap[] = {0, 1, 4, 2, 3, 6, 5};
		REQUIRE(probe.getHeap() == vector<size_t>(begin(postHeap), end(postHeap)));
	}
	
	SECTION("Heapify up.") {
		size_t preHeap[] = {1, 2, 3, 4, 0, 5, 6};
		SizeTPriorityQueueProbe probe(vector<size_t>(begin(preHeap), end(preHeap)));
		probe.heapifyUp(4);
		
		size_t postHeap[] = {0, 1, 3, 4, 2, 5, 6};
		REQUIRE(probe.getHeap() == vector<size_t>(begin(postHeap), end(postHeap)));
	}
	
	SECTION("Reverse heap build.") {
		size_t preHeap[] = {7, 6, 5, 4, 3, 2, 1, 0};
		SizeTPriorityQueueProbe probe(vector<size_t>(begin(preHeap), end(preHeap)));
		probe.buildHeap();
		
		size_t postHeap[] = {0, 3, 1, 4, 7, 2, 5, 6};
		REQUIRE(probe.getHeap() == vector<size_t>(begin(postHeap), end(postHeap)));
	}
	
	SECTION("Update up.") {
		int values[] = {1, 2, 3, 4, 5, 6, 7};
		IntValue valueObjs[7];
		vector<IntValue *> valueObjPtrs;
		for(size_t i = 0; i < 7; i++) {
			valueObjs[i].ptr = &(values[i]);
			valueObjPtrs.push_back(&valueObjs[i]);
		}
		
		IntValuePriorityQueueProbe probe(valueObjPtrs);
		values[5] = 0;
		probe.update(&valueObjs[5]);
		
		size_t postHeapIndexes[] = {5, 1, 0, 3, 4, 2, 6};
		vector<IntValue *> postHeap;
		for(size_t i = 0; i < 7; i++) {
			postHeap.push_back(valueObjPtrs[postHeapIndexes[i]]);
		}
		REQUIRE(probe.getHeap() == vector<IntValue *>(begin(postHeap), end(postHeap)));
	}
	
	SECTION("Update down.") {
		int values[] = {1, 2, 3, 4, 6, 7, 8};
		IntValue valueObjs[7];
		vector<IntValue *> valueObjPtrs;
		for(size_t i = 0; i < 7; i++) {
			valueObjs[i].ptr = &(values[i]);
			valueObjPtrs.push_back(&valueObjs[i]);
		}
		
		IntValuePriorityQueueProbe probe(valueObjPtrs);
		values[0] = 5;
		probe.update(&valueObjs[0]);
		
		size_t postHeapIndexes[] = {1, 3, 2, 0, 4, 5, 6};
		vector<IntValue *> postHeap;
		for(size_t i = 0; i < 7; i++) {
			postHeap.push_back(valueObjPtrs[postHeapIndexes[i]]);
		}
		REQUIRE(probe.getHeap() == vector<IntValue *>(begin(postHeap), end(postHeap)));
	}
	
	SECTION("Add already present.") {
		size_t preHeap[] = {0, 1, 2, 3, 4, 5, 6};
		SizeTPriorityQueueProbe probe(vector<size_t>(begin(preHeap), end(preHeap)));
		
		REQUIRE(!probe.add(3));
		
		size_t postHeap[] = {0, 1, 2, 3, 4, 5, 6};
		REQUIRE(probe.getHeap() == vector<size_t>(begin(postHeap), end(postHeap)));
	}
	
	SECTION("Add not present, heapify not needed.") {
		size_t preHeap[] = {0, 1, 2, 3, 4, 5, 6};
		SizeTPriorityQueueProbe probe(vector<size_t>(begin(preHeap), end(preHeap)));
		
		REQUIRE(probe.add(7));
		
		size_t postHeap[] = {0, 1, 2, 3, 4, 5, 6, 7};
		REQUIRE(probe.getHeap() == vector<size_t>(begin(postHeap), end(postHeap)));
	}
	
	SECTION("Add not present, heapify needed.") {
		size_t preHeap[] = {0, 1, 2, 4, 5, 6, 7};
		SizeTPriorityQueueProbe probe(vector<size_t>(begin(preHeap), end(preHeap)));
		
		REQUIRE(probe.add(3));
		
		size_t postHeap[] = {0, 1, 2, 3, 5, 6, 7, 4};
		REQUIRE(probe.getHeap() == vector<size_t>(begin(postHeap), end(postHeap)));
	}
	
	SECTION("Remove not present.") {
		size_t preHeap[] = {0, 1, 2, 3, 4, 5, 6};
		SizeTPriorityQueueProbe probe(vector<size_t>(begin(preHeap), end(preHeap)));
		
		REQUIRE(!probe.remove(7));
		
		size_t postHeap[] = {0, 1, 2, 3, 4, 5, 6};
		REQUIRE(probe.getHeap() == vector<size_t>(begin(postHeap), end(postHeap)));
	}
	
	SECTION("Remove present, no heapify needed.") {
		size_t preHeap[] = {0, 1, 2, 3, 4, 5, 6};
		SizeTPriorityQueueProbe probe(vector<size_t>(begin(preHeap), end(preHeap)));
		
		REQUIRE(probe.remove(3));
		
		size_t postHeap[] = {0, 1, 2, 6, 4, 5};
		REQUIRE(probe.getHeap() == vector<size_t>(begin(postHeap), end(postHeap)));
	}
	
	SECTION("Remove present, heapify needed.") {
		size_t preHeap[] = {0, 1, 2, 3, 4, 5, 6};
		SizeTPriorityQueueProbe probe(vector<size_t>(begin(preHeap), end(preHeap)));
		
		REQUIRE(probe.remove(1));
		
		size_t postHeap[] = {0, 3, 2, 6, 4, 5};
		REQUIRE(probe.getHeap() == vector<size_t>(begin(postHeap), end(postHeap)));
	}
	
	SECTION("Construct a bunch of random queues.") {
		
		size_t count = 10000;
		size_t maxSize = 100;
		
		uniform_int_distribution<size_t> sizeDist(0, maxSize);
		for(size_t i = 0; i < count; i++) {
			size_t size = sizeDist(rng);
			vector<size_t> preHeap;
			for(size_t j = 0; j < size; j++) {
				preHeap.push_back(j);
			}
			std::shuffle(preHeap.begin(), preHeap.end(), rng);
			
			SizeTPriorityQueueProbe probe(preHeap);
			probe.buildHeap();
			REQUIRE(probe.verifyHeap());
		}
	}
	
	SECTION("Construct a bunch of random queues and then randomly add/remove new items from them") {
		
		size_t count = 10000;
		size_t maxSize = 100;
		size_t nModifications = 100;
		
		uniform_int_distribution<size_t> sizeDist(0, maxSize);
		for(size_t i = 0; i < count; i++) {
			size_t size = sizeDist(rng);
			
			size_t nextId = 0 ;
			vector<size_t> active;
			for(size_t j = 0; j < size; j++) {
				active.push_back(nextId++);
			}
			std::shuffle(active.begin(), active.end(), rng);
			
			SizeTPriorityQueueProbe probe(active);
			probe.buildHeap();
			
			bernoulli_distribution flipCoin;
			for(size_t j = 0; j < nModifications; j++) {
				if(active.size() == 0 || flipCoin(rng)) {
					active.push_back(nextId++);
					probe.add(active.back());
				}
				else {
					uniform_int_distribution<size_t> indexDist(0, active.size() - 1);
					size_t index = indexDist(rng);
					size_t obj = active[index];
					active[index] = active.back();
					active.pop_back();
					probe.remove(obj);
				}
			}
			
			REQUIRE(probe.verifyHeap());
		}
	}
}
