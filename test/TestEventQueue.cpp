//
//  TestEventQueue.cpp
//  malariamodel
//
//  Created by Ed Baskerville on 2/21/14.
//  Copyright (c) 2014 Ed Baskerville. All rights reserved.
//

#include "catch.hpp"
#include "zppsim_random.hpp"
#include "EventQueue.hpp"

using namespace std;
using namespace zppsim;

TEST_CASE("EventQueue behaves properly", "[EventQueue]")
{
	rng_t rng(100);
	
	SECTION("Approximately the right number of events for rate = 1.") {
		unique_ptr<RateEvent> event(new RateEvent(1.0, 0.0, rng));
		vector<Event *> events;
		events.push_back(event.get());
		
		EventQueue sampler(events, &rng);
		while(sampler.getTime() < 1000.0)
		{
			Event * event;
			double dt;
			sampler.performNextEvent(event, dt);
		}
		REQUIRE(sampler.getEventCount() > 900);
		REQUIRE(sampler.getEventCount() < 1100);
		REQUIRE(sampler.getEventCount() == event->getCount());
	}
	
	SECTION("Approximately the right number of events for rate = 2.") {
		unique_ptr<RateEvent> event(new RateEvent(2.0, 0.0, rng));
		vector<Event *> events;
		events.push_back(event.get());
		
		EventQueue sampler(events, &rng);
		while(sampler.getTime() < 500.0)
		{
			Event * event;
			double dt;
			sampler.performNextEvent(event, dt);
		}
		REQUIRE(sampler.getEventCount() > 900);
		REQUIRE(sampler.getEventCount() < 1100);
		REQUIRE(sampler.getEventCount() == event->getCount());
	}
	
	SECTION("Approximately the right number of events for two equal-rate events") {
		vector<Event *> events;
		unique_ptr<RateEvent> event1(new RateEvent(1.0, 0.0, rng));
		events.push_back(event1.get());
		unique_ptr<RateEvent> event2(new RateEvent(1.0, 0.0, rng));
		events.push_back(event2.get());
		
		EventQueue sampler(events, &rng);
		while(sampler.getTime() < 1000.0)
		{
			Event * event;
			double dt;
			sampler.performNextEvent(event, dt);
		}
		REQUIRE(event1->getCount() > 900);
		REQUIRE(event1->getCount() < 1100);
		REQUIRE(event2->getCount() > 900);
		REQUIRE(event2->getCount() < 1100);
		REQUIRE(sampler.getEventCount() == event1->getCount() + event2->getCount());
	}
	
	SECTION("Approximately the right number of events for two different-rate events") {
		vector<Event *> events;
		unique_ptr<RateEvent> event1(new RateEvent(2.0, 0.0, rng));
		events.push_back(event1.get());
		unique_ptr<RateEvent> event2(new RateEvent(1.0, 0.0, rng));
		events.push_back(event2.get());
		
		EventQueue sampler(events, &rng);
		while(sampler.getTime() < 1000.0)
		{
			Event * event;
			double dt;
			sampler.performNextEvent(event, dt);
		}
		REQUIRE(event1->getCount() > 1800);
		REQUIRE(event1->getCount() < 2200);
		REQUIRE(event2->getCount() > 900);
		REQUIRE(event2->getCount() < 1100);
		REQUIRE(sampler.getEventCount() == event1->getCount() + event2->getCount());
	}
	
	SECTION("Approximately the right number of events before and after rate change") {
		RateEvent event(1.0, 0.0, rng);
		vector<Event *> events(1, &event);
		
		EventQueue sampler(events, &rng);
		while(sampler.getTime() < 1000.0)
		{
			Event * event;
			double dt;
			sampler.performNextEvent(event, dt);
		}
		REQUIRE(event.getCount() > 900);
		REQUIRE(event.getCount() < 1100);
		
		sampler.resetEventCount();
		event.resetCount();
		
		event.setRate(sampler, 2.0);
		sampler.updateEvent(&event);
		while(sampler.getTime() < 1500.0)
		{
			Event * event;
			double dt;
			sampler.performNextEvent(event, dt);
		}
		REQUIRE(event.getCount() > 900);
		REQUIRE(event.getCount() < 1100);
		REQUIRE(sampler.getEventCount() == event.getCount());
	}
	
	SECTION("Approximately the right number of events before and after several rate changes with two events.") {
		RateEvent event1(1.0, 0.0, rng);
		RateEvent event2(1.0, 0.0, rng);
		vector<Event *> events;
		events.push_back(&event1);
		events.push_back(&event2);
		
		EventQueue sampler(events, &rng);
		
		Event * event;
		double dt;
		while(sampler.getTime() < 1000.0)
		{
			sampler.performNextEvent(event, dt);
		}
		REQUIRE(event1.getCount() > 900);
		REQUIRE(event1.getCount() < 1100);
		REQUIRE(event2.getCount() > 900);
		REQUIRE(event2.getCount() < 1100);
		REQUIRE(sampler.getEventCount() == event1.getCount() + event2.getCount());
		
		sampler.resetEventCount();
		event1.resetCount();
		event2.resetCount();
		
		event1.setRate(sampler, 2.0);
		sampler.updateEvent(&event1);
		
		while(sampler.getTime() < 2000.0)
		{
			sampler.performNextEvent(event, dt);
		}
		REQUIRE(event1.getCount() > 1800);
		REQUIRE(event1.getCount() < 2200);
		REQUIRE(event2.getCount() > 900);
		REQUIRE(event2.getCount() < 1100);
		
		sampler.resetEventCount();
		event1.resetCount();
		event2.resetCount();
		
		event2.setRate(sampler, 2.0);
		sampler.updateEvent(&event2);
		
		while(sampler.getTime() < 2500.0)
		{
			sampler.performNextEvent(event, dt);
		}
		REQUIRE(event1.getCount() > 900);
		REQUIRE(event1.getCount() < 1100);
		REQUIRE(event2.getCount() > 900);
		REQUIRE(event2.getCount() < 1100);
		
		sampler.resetEventCount();
		event1.resetCount();
		event2.resetCount();
		
		event1.setRate(sampler, 0.5);
		sampler.updateEvent(&event1);
		event2.setRate(sampler, 0.5);
		sampler.updateEvent(&event2);
		
		while(sampler.getTime() < 4500.0)
		{
			sampler.performNextEvent(event, dt);
		}
		REQUIRE(event1.getCount() > 900);
		REQUIRE(event1.getCount() < 1100);
		REQUIRE(event2.getCount() > 900);
		REQUIRE(event2.getCount() < 1100);
		
		sampler.resetEventCount();
		event1.resetCount();
		event2.resetCount();
		
		event1.setRate(sampler, 0.5);
		sampler.updateEvent(&event1);
		event2.setRate(sampler, 5.0);
		sampler.updateEvent(&event2);
		
		while(sampler.getTime() < 6500.0)
		{
			sampler.performNextEvent(event, dt);
		}
		REQUIRE(event1.getCount() > 900);
		REQUIRE(event1.getCount() < 1100);
		REQUIRE(event2.getCount() > 9000);
		REQUIRE(event2.getCount() < 11000);
	}
	
	SECTION("Approximately the right number of events before and after many rate changes with many events.") {
		vector<unique_ptr<RateEvent>> events;
		vector<Event *> eventPtrs;
		for(size_t i = 0; i < 101; i++) {
			events.push_back(unique_ptr<RateEvent>(new RateEvent(1.0, 0.0, rng)));
			eventPtrs.push_back(events[i].get());
		}
		
		EventQueue sampler(eventPtrs, &rng);
		uniform_int_distribution<size_t> ud(0, 100);
		size_t specialId = 0;
		for(int i = 0; i < 100; i++) {
			events[specialId]->setRate(sampler, 1.0);
			sampler.updateEvent(eventPtrs[specialId]);
			specialId = ud(rng);
			events[specialId]->setRate(sampler, 100.0);
			sampler.updateEvent(eventPtrs[specialId]);
			while(sampler.getTime() < 10.0 * (i+1)) {
				Event * event;
				double dt;
				sampler.performNextEvent(event, dt);
			}
			REQUIRE(sampler.getEventCount() > 1800);
			REQUIRE(sampler.getEventCount() < 2200);
			REQUIRE(events[specialId]->getCount() > 900);
			REQUIRE(events[specialId]->getCount() < 1100);
			sampler.resetEventCount();
			for(auto & eventPtr : events) {
				eventPtr->resetCount();
			}
		}
	}
}
