#ifndef __zppsim_util__
#define __zppsim_util__

#include <vector>
#include <unordered_set>
#include <unordered_map>

namespace zppsim {

std::vector<size_t> makeRange(size_t from, size_t to);
std::vector<size_t> makeRange(size_t size);

double add(std::vector<double> const & vec);
std::vector<double> addCumulative(std::vector<double> const & vec);

//template<typename T>
//using unordered_set_bh = std::unordered_set<T, boost::hash<T>>;
//
//template<typename K, typename V>
//using unordered_map_bh = std::unordered_map<K, V, boost::hash<K>>;

} // namespace zppsim

#endif
