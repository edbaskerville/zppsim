#ifndef __zppsim__EventQueue__
#define __zppsim__EventQueue__

#include <vector>
#include "zppsim_random.hpp"
#include "IndexedPriorityQueue.hpp"

namespace zppsim {

class EventQueue;

class Event
{
public:
	Event(double time);
	
	double getTime();
	void setTime(EventQueue & queue, double time);
	
    virtual std::string eventName();
	virtual void performEvent(EventQueue & queue) = 0;
	virtual void finalizeEvent(EventQueue & queue);
protected:
	double _time;
};

class PeriodicEvent : public Event
{
public:
	PeriodicEvent(double initialTime, double period);
	void finalizeEvent(EventQueue & queue);
private:
	double initialTime;
	double period;
	int64_t count;
};

class OneTimeEvent : public Event
{
public:
	OneTimeEvent(double time);
	virtual void finalizeEvent(EventQueue & queue);
};

class RateEvent : public Event
{
public:
	RateEvent(double rate, double initTime, rng_t & rng);
	RateEvent(double time);
	void setRate(EventQueue & queue, double newRate);
	double getRate() const;
	virtual void finalizeEvent(EventQueue & queue);
	
	virtual void performEvent(EventQueue & queue) = 0;
	
protected:
	double rate;
	double sampleTime(double initTime, rng_t & rng);
};

class CompareEventPointers
{
public:
	bool operator()(Event * ep1, Event * ep2)
	{
		return ep1->getTime() < ep2->getTime();
	}
};

typedef IndexedPriorityQueue<Event *, nullptr, std::hash<Event *>, CompareEventPointers> EventPtrQueue;

class EventQueue
{
public:
	EventQueue(rng_t & rng);
	EventQueue(rng_t & rng, void * context);
	EventQueue(rng_t & rng, std::vector<Event *> const & initEvents);
	EventQueue(rng_t & rng, std::vector<Event *> const & initEvents, void * context);
	
	void addEvent(Event * event);
	void removeEvent(Event * event);
	void updateEvent(Event * event);
	
	void * getContext();
	rng_t * getRngPtr();
	
	bool performNextEvent(Event * & eventOut, double & dtOut);
	double getTime();
	double getNextTime();
	
	int64_t getEventCount();
	void resetEventCount();
	
	int64_t size();
	bool containsEvent(Event * const event);
	
	bool verify();

private:
	rng_t * rngPtr;
	
	double time;
	int64_t eventCount;
	
	// Indexed priority queue of event indexes sorted by time
	EventPtrQueue queue;
	
	void * context;
	
	std::vector<Event *> initializeEvents(std::vector<Event *> const & events);
};

} // namespace zppsim

#endif
