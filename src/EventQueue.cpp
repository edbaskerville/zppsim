#include "EventQueue.hpp"

#include <vector>
#include <random>
#include <cassert>
#include <iostream>

using namespace std;

namespace zppsim {

/*** EVENT SUPERCLASS IMPLEMENTATIONS ***/

Event::Event(double time): _time(time)
{
}

double Event::getTime()
{
	return _time;
}

void Event::setTime(EventQueue & queue, double time)
{
	_time = time;
	queue.updateEvent(this);
}

void Event::finalizeEvent(EventQueue & queue)
{
}

std::string Event::eventName()
{
    return "eventName not overridden";
}

PeriodicEvent::PeriodicEvent(double initialTime, double period) :
	Event(initialTime), initialTime(initialTime), period(period), count(0)
{
}

void PeriodicEvent::finalizeEvent(EventQueue & queue)
{
	count++;
	setTime(queue, initialTime + period * count);
}

OneTimeEvent::OneTimeEvent(double time) : Event(time) { }

void OneTimeEvent::finalizeEvent(EventQueue & queue)
{
	queue.removeEvent(this);
}

RateEvent::RateEvent(double rate, double initTime, rng_t & rng):
	Event(std::numeric_limits<double>::infinity()), rate(rate)
{
	_time = sampleTime(initTime, rng);
}

RateEvent::RateEvent(double time):
	Event(time), rate(std::numeric_limits<double>::infinity())
{
}

double RateEvent::getRate() const
{
	return rate;
}

void RateEvent::setRate(EventQueue & queue, double newRate)
{
	assert(newRate >= 0);
	
	double changeTime = queue.getTime();
	double oldTime = getTime();
	
	// If changeTime is the same as the time of this event,
	// then this event is being performed and a new time doesn't
	// need to be sampled yet.
	if(changeTime == oldTime) {
		rate = newRate;
	}
	// Otherwise, rescale the old time
	else {
		assert(changeTime < oldTime || std::isinf(newRate));
		
		double oldRate = rate;
		if(oldRate != newRate) {
			rate = newRate;
			
			// If the new rate is 0, then the event will never happen.
			double newTime;
			if(rate == 0) {
				newTime = std::numeric_limits<double>::infinity();
			}
			// If the new rate is infinite, then the event happens right now.
			else if(std::isinf(newRate)) {
				newTime = changeTime;
			}
			// If the old rate was 0, then we need to sample a new time from scratch.
			else if(oldRate == 0 || std::isinf(oldRate)) {
				newTime = sampleTime(changeTime, *(queue.getRngPtr()));
			}
			// If the old rate was nonzero, we can rescale the previously generated time.
			else {
				newTime = changeTime + (oldTime - changeTime) * oldRate / rate;
			}
			setTime(queue, newTime);
		}
	}
}

void RateEvent::finalizeEvent(EventQueue & queue)
{
	setTime(queue, sampleTime(queue.getTime(), *(queue.getRngPtr())));
}

double RateEvent::sampleTime(double initTime, rng_t & rng)
{
	assert(rate >= 0.0);
	if(rate == 0.0) {
		return std::numeric_limits<double>::infinity();
	}
	else if(std::isinf(rate)) {
		return initTime;
	}
	
	std::exponential_distribution<> timeDist(rate);
	return initTime + timeDist(rng);
}

/*** EVENT QUEUE IMPLEMENTATION ***/

EventQueue::EventQueue(rng_t & rng):
	rngPtr(&rng), time(0.0), eventCount(0), queue(), context(nullptr)
{
}

EventQueue::EventQueue(rng_t & rng, void * context):
	rngPtr(&rng), time(0.0), eventCount(0), queue(), context(context)
{
}

EventQueue::EventQueue(rng_t & rng, std::vector<Event *> const & initEvents):
	rngPtr(&rng), time(0.0), eventCount(0), queue(initEvents), context(nullptr)
{
}

EventQueue::EventQueue(rng_t & rng, std::vector<Event *> const & initEvents, void * context):
	rngPtr(&rng), time(0.0), eventCount(0), queue(initEvents), context(context)
{
}

void * EventQueue::getContext()
{
	return context;
}

rng_t * EventQueue::getRngPtr()
{
	return rngPtr;
}

void EventQueue::addEvent(Event * event)
{
	queue.add(event);
    assert(event->getTime() >= time);
	if(size() < 0) {
		throw runtime_error("signed 64-bit integer overflow");
	}
}

void EventQueue::removeEvent(Event * event)
{
	queue.remove(event);
}

void EventQueue::updateEvent(Event * event)
{
	queue.update(event);
}

double EventQueue::getTime()
{
	return time;
}

double EventQueue::getNextTime()
{
	if(queue.size() == 0) {
		return numeric_limits<double>::infinity();
	}
	return queue.getHead()->getTime();
}

bool EventQueue::performNextEvent(Event * & eventOut, double & dtOut)
{
	Event * event = queue.getHead(); 
	double eventTime = event->getTime();
	assert(eventTime >= time);
	
	dtOut = eventTime - time;
	time = eventTime;
	
	// If all event times are infinite, the simulation is done: return false.
	if(time == numeric_limits<double>::infinity()) {
		return false;
	}
	
	// Otherwise perform the event and return its value in the out parameter.
	else {
		event->performEvent(*this);
		if(queue.contains(event)) {
			event->finalizeEvent(*this);
		}
		eventOut = event;
		
		eventCount++;
		
		return true;
	}
}

int64_t EventQueue::getEventCount()
{
	if(eventCount < 0) {
		throw std::runtime_error("signed 64-bit integer underflow error");
	}
	return eventCount;
}

void EventQueue::resetEventCount()
{
	eventCount = 0;
}

int64_t EventQueue::size()
{
	return queue.size();
}

bool EventQueue::containsEvent(Event * event)
{
	return queue.contains(event);
}

bool EventQueue::verify()
{
	return queue.verifyHeap();
}

} // namespace zppsim
