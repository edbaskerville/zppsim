//
//  IndexedSet.h
//  zppsim
//
//  Created by Ed Baskerville on 5/21/14.
//  Copyright (c) 2014 Ed Baskerville. All rights reserved.
//

#ifndef __zppsim__IndexedSet__
#define __zppsim__IndexedSet__

#include "zppsim_util.hpp"

namespace zppsim {

template<typename T, typename Hash=std::hash<T>>
class IndexedSet
{
public:
	bool insert(T const & item)
	{
		if(!contains(item)) {
			// Just add item to the end of the vector and record its location there
			vec.push_back(item);
			indexMap[item] = vec.size() - 1;
			return true;
		}
		return false;
	}
	
	bool remove(T const & item)
	{
		if(contains(item)) {
			// Take the item at the end of the vector and move it where this item was,
			// then pop off the end of the vector.
			size_t index = indexMap[item];
			if(index != vec.size() - 1) {
				T oldBack = vec.back();
				vec[index] = oldBack;
				indexMap[oldBack] = index;
			}
			indexMap.erase(item);
			vec.pop_back();
			return true;
		}
		return false;
	}
	
	bool contains(T const & item)
	{
		return indexMap.find(item) != indexMap.end();
	}
	
	size_t indexOf(T const & item)
	{
		if(contains(item)) {
			return indexMap[item];
		}
		return vec.size();
	}
	
	T itemAtIndex(size_t index)
	{
		return vec[index];
	}
	
	size_t size()
	{
		return vec.size();
	}
	
	void verify()
	{
		assert(vec.size() == indexMap.size());
		for(size_t i = 0; i < vec.size(); i++) {
			assert(indexMap[vec[i]] == i);
		}
	}
private:
	std::vector<T> vec;
	std::unordered_map<T, size_t, Hash> indexMap;
};

} // namespace zppsim

#endif
