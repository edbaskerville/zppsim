#include "Simulation.h"
#include <iostream>

using namespace std;
using namespace zppsim;

namespace zppsim
{
namespace example
{

Simulation::Simulation() :
	rng(1000),
	queuePtr(new EventQueue(rng)),
	popSize(1000),
	birthEventPtr(new BirthEvent(this, 0.0, numeric_limits<double>::infinity(), rng)),
	deathEventPtr(new DeathEvent(this, 0.0, numeric_limits<double>::infinity(), rng))
{
	queuePtr->addEvent(birthEventPtr.get());
	queuePtr->addEvent(deathEventPtr.get());
	updateRates();
}

void Simulation::runUntil(double time)
{
	while(queuePtr->getNextTime() <= time) {
		runOneEvent();
	}
}

void Simulation::runOneEvent()
{
	Event * event;
	double dt;
	queuePtr->performNextEvent(event, dt);
	cerr << "time " << queuePtr->getTime() << endl;
}

void Simulation::performBirth()
{
	assert(popSize < numeric_limits<size_t>::max());
	popSize++;
	updateRates();
	
	cerr << "birth, pop = " << popSize << endl;
}

void Simulation::performDeath()
{
	assert(popSize > 0);
	popSize--;
	updateRates();
	
	cerr << "death, pop = " << popSize << endl;
}

void Simulation::updateRates()
{
	double birthRate = popSize > 1000 ? 0 : (1000 - popSize) * 0.5;
	birthEventPtr->setRate(*queuePtr, birthRate);
	
	double deathRate = popSize * 0.1;
	deathEventPtr->setRate(*queuePtr, deathRate);
}


SimEvent::SimEvent(Simulation * simPtr, double initRate, double initTime, rng_t & rng) :
	RateEvent(initRate, initTime, rng),
	simPtr(simPtr)
{
}

BirthEvent::BirthEvent(Simulation * simPtr, double initRate, double initTime, rng_t & rng) :
	SimEvent(simPtr, initRate, initTime, rng)
{
}

void BirthEvent::performEvent(EventQueue & queue)
{
	simPtr->performBirth();
}

DeathEvent::DeathEvent(Simulation * simPtr, double initRate, double initTime, rng_t & rng) :
	SimEvent(simPtr, initRate, initTime, rng)
{
}

void DeathEvent::performEvent(EventQueue & queue)
{
	simPtr->performDeath();
}

} // namespace example
} // namespace zppsim
