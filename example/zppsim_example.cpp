#include <memory>
#include <iostream>
#include "Simulation.h"

using namespace std;
using namespace zppsim::example;

int main(int const argc, char const * const * const argv)
{
	unique_ptr<Simulation> sim(new Simulation());
	sim->runUntil(100.0);
}
