#ifndef __zppsim_example__Simulation__
#define __zppsim_example__Simulation__

#include <memory>
#include "zppsim_random.hpp"
#include "EventQueue.hpp"

namespace zppsim
{
namespace example
{

class Simulation;

class SimEvent : public RateEvent
{
public:
	SimEvent(Simulation * simPtr, double initRate, double initTime, rng_t & rng);
	
	Simulation * simPtr;
};

class BirthEvent : public SimEvent
{
public:
	BirthEvent(Simulation * simPtr, double initRate, double initTime, rng_t & rng);
	virtual void performEvent(EventQueue & queue);
};

class DeathEvent : public SimEvent
{
public:
	DeathEvent(Simulation * simPtr, double initRate, double initTime, rng_t & rng);
	void performEvent(EventQueue & queue);
};

class Simulation
{
public:
	Simulation();
	
	void runUntil(double time);
	void runOneEvent();
	
	void performBirth();
	void performDeath();
	
	void updateRates();
	
	std::unique_ptr<zppsim::EventQueue> queuePtr;
	zppsim::rng_t rng;
	
	size_t popSize;
	
	std::unique_ptr<BirthEvent> birthEventPtr;
	std::unique_ptr<DeathEvent> deathEventPtr;
};

} // namespace example
} // namespace zppsim

#endif /* defined(__zppsim_example__Simulation__) */
